/*
 * Copyright (c) 2014-2017.  Moxie Software. All Rights Reserved.
 */

package com.gomoxie.mylibrary;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class ProactiveOfferLayout extends LinearLayout {
    public ProactiveOfferLayout(Context context) {
        super(context);
    }

    public ProactiveOfferLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProactiveOfferLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ProactiveOfferLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    ProactiveOffer offer;

    public ProactiveOffer getOffer() {
        return offer;
    }

    public void setOffer(ProactiveOffer offer) {
        this.offer = offer;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        // When the wrapper view is detached from the window, make sure we clear the reference
        //  to it to avoid leaking context memory
        offer.layout = null;
    }
}
