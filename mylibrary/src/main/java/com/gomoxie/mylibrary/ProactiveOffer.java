/*
 * Copyright (c) 2014-2017.  Moxie Software. All Rights Reserved.
 */

package com.gomoxie.mylibrary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Display a Proactive Concierge-like offer
 */
public class ProactiveOffer {

    // We insure that this is safe by clearing our reference to the view hierarchy when it
    //  gets detached from the window.  Since the views get detached before the activity
    //  is paused, we're protected from ever having an activity context reference cycle
    @SuppressLint("StaticFieldLeak")
    @Nullable
    static ProactiveOffer currentOffer;

    @NonNull
    String title;

    @NonNull
    String message;

    @NonNull
    OnOfferAccepted onOfferAccepted;

    @NonNull
    String actionLabel;

    @Nullable
    Drawable drawable;

    @Nullable
    ProactiveOfferLayout layout;

    /**
     * Call back when offer has been accepted
     */
    public interface OnOfferAccepted {
        /**
         * Offer has been accepted, take the appropriate action
         */
        void onOfferAccepted();
    }

    /**
     * Create a ProactiveOffer
     *
     * @param title offer title
     * @param message offer body text
     * @return partially constructed (and not displayed) ProactiveOffer
     */
    @NonNull
    static public ProactiveOffer create(@NonNull String title, @NonNull String message) {
        return new ProactiveOffer(title, message);
    }

    private ProactiveOffer(@NonNull String title, @NonNull String message) {
        this.title = title;
        this.message = message;
        //noinspection ConstantConditions
        this.actionLabel = null;
        //noinspection ConstantConditions
        this.onOfferAccepted = null;
    }

    /**
     * Set a drawable to be displayed as the ProactiveOffer icon
     *
     * @param drawable drawable to be displayed
     * @return partially constructed ProactiveOffer
     */
    @NonNull
    public ProactiveOffer setDrawable(@Nullable Drawable drawable) {
        this.drawable = drawable;
        return this;
    }

    /**
     * set the action label and handler
     *
     * @param offerLabel label for the action button
     * @param onOfferAccepted action handler if action button is used (ie., offer is accepted)
     * @return partially constructed ProactiveOffer
     */
    @NonNull
    public ProactiveOffer setAction(@NonNull String offerLabel, @NonNull OnOfferAccepted onOfferAccepted) {
        this.actionLabel = offerLabel;
        this.onOfferAccepted = onOfferAccepted;
        return this;
    }

    /**
     * Display a partially constructed proactive offer
     *
     * <p>
     *     <strong>Note: </strong>the new offer will only be presented if there is currently
     *     no outstanding offer displayed.
     * </p>
     *
     * @return displayed ProactiveOffer
     */
    @Nullable
    public ProactiveOffer show(Activity activity) {
        ViewGroup parent = (ViewGroup) activity.findViewById(android.R.id.content);

        layout = (ProactiveOfferLayout) LayoutInflater.from(activity).inflate(R.layout.moxie_proactive_offer, parent, false);
        layout.setOffer(this);

        TextView message = (TextView) layout.findViewById(R.id.moxie_offer_message);
        ImageView imageView = (ImageView) layout.findViewById(R.id.moxie_offer_image);
        Button action = (Button) layout.findViewById(R.id.moxie_offer_action);
        Toolbar toolbar = (Toolbar) layout.findViewById(R.id.toolbar_actionbar);

        toolbar.setTitle(this.title);
        message.setText(this.message);

        if(this.drawable != null) {
            imageView.setImageDrawable(this.drawable);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }

        if(this.onOfferAccepted != null) {
            action.setText(this.actionLabel);
            action.setVisibility(View.VISIBLE);
            action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProactiveOffer.this.dismiss();
                    ProactiveOffer.this.onOfferAccepted.onOfferAccepted();
                }
            });
        } else {
            action.setVisibility(View.GONE);
        }

        toolbar.inflateMenu(R.menu.moxie_menu_proactive);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int itemId = item.getItemId();

                if(itemId == R.id.action_cancel) {
                    ProactiveOffer.this.dismiss();
                    return true;
                } else {
                    return false;
                }
            }
        });

        // Figure out where to display it (we want it below the action bar, which may or may
        //  not be in the android container view
        FrameLayout.LayoutParams frameLayout = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                Gravity.TOP
        );

        // If the host activity is using the space under the status bar, we want to move down
        //  out from under it so add a status bar height top margin
        int[] location = new int[2];
        parent.getLocationOnScreen(location);
        if(location[1] == 0) {
            frameLayout.setMargins(0, Util.getStatusBarHeight(activity), 0, 0);
        }

        // Animate the view into position from completely off screen.  This has to be done
        //  in the VTO because the height isn't set until after layout time.
        layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Animation animation = new TranslateAnimation(0.0f, 0.0f, -layout.getHeight(), 0.0f);
                animation.setDuration(500);
                layout.startAnimation(animation);
                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        parent.addView(layout, frameLayout);

        currentOffer = this;

        return this;
    }

    /**
     * Redisplay an offer that has been lost due to rotation, etc.
     *
     * @param activity Activity in which to redisplay the offer
     */
    private void redisplay(Activity activity) {
        // If this offer is already being displayed, redisplay it (in case of rotation, etc.)
        if(layout != null) {
            ViewGroup parent = (ViewGroup) layout.getParent();
            if (parent != null) {
                parent.removeView(layout);
                layout = null;
            }
        }

        show(activity);
    }

    /**
     * Dismiss the proactive offer without taking any action
     */
    public void dismiss() {
        if(layout != null) {
            // Animate the offer off screen and then remove it
            final ViewGroup save = layout;

            Animation animation = new TranslateAnimation(0.0f, 0.0f, 0.0f, -layout.getHeight());
            animation.setDuration(500);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    // It's gone, now remove it from the hierarchy
                    ((ViewGroup) save.getParent()).removeView(save);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            layout.startAnimation(animation);

            layout = null;
        }
        if(currentOffer == this) {
            currentOffer = null;
        }
    }

    /**
     * Redisplay any pending ProactiveOffers.
     * <p>
     *     This routine will need to be called if the view hierarchy has been reconstructed for
     *     any reason, including normal Android processing of rotation
     *     ({@link Activity#onPause()}/{@link Activity#onResume()})
     * </p>
     *
     * @param activity activity in which to redisplay offers
     */
    public static void redisplayOffers(Activity activity) {
        if(currentOffer != null) {
            currentOffer.redisplay(activity);
        }
    }

    /**
     * Cancel any pending ProactiveOffers.
     * <p>
     *     This routine will need to be called if the underlying Activity is cancelled/closed
     *     before a ProactiveOffer has been handled.
     * </p>
     */
    public static void cancelOffers() {
        if(currentOffer != null) {
            currentOffer.dismiss();
            currentOffer = null;
        }
    }

}
