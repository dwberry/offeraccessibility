/*
 * Copyright (c) 2014-2017.  Moxie Software. All Rights Reserved.
 */

package com.gomoxie.mylibrary;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;

/**
 * Created by equesada on 4/9/15.
 */
public class Util {
    static public int getStatusBarHeight(@NonNull Context context) {
        int result = 0;
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
